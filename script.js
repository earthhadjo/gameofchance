let images = ["src/rock.png", "src/paper.jpg", "src/scissors.jpg"];
const image = document.createElement("img");
const buttons = document.querySelectorAll('.choice');
const computer = document.getElementById("computer");
const choices = ["rock", "paper", "scissors"];
computer.appendChild(image)

function getComputerChoice() {
    let choice = Math.floor(Math.random() * 3);
    image.src = images[choice];
    return choices[choice]
}

for (let button of buttons) { button.addEventListener("click", clickHandler) }

function clickHandler(event) {
    let computerChoice = getComputerChoice();
    let playerChoice = event.target.id;
    findWinner(computerChoice, playerChoice);
}

function findWinner(computerChoice, playerChoice){
    if (computerChoice === playerChoice) {
        alert("Tie")
        
    } else if (computerChoice === "rock" && playerChoice === "scissors"
            || computerChoice === "paper" && playerChoice === "rock"
            || computerChoice === "scissors" && playerChoice === "paper"){
        alert("ComputerWins")
       
    }else alert("PlayerWins")
}